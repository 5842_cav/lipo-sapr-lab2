/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   TestsforCfile.h
 * Author: Анастасия
 *
 * Created on 25.09.2017, 21:10:28
 */

#ifndef TESTSFORCFILE_H
#define TESTSFORCFILE_H

#include <cppunit/extensions/HelperMacros.h>

class TestsforCfile : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(TestsforCfile);

    CPPUNIT_TEST(testError);
    CPPUNIT_TEST(testConfig);
    CPPUNIT_TEST(testAnotherlog);
    
    CPPUNIT_TEST_SUITE_END();

public:
    TestsforCfile();
    virtual ~TestsforCfile();
    void setUp();
    void tearDown();

private:
    void testError();
    void testConfig();
    void testAnotherlog();
};

#endif /* TESTSFORCFILE_H */

