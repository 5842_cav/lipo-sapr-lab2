/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   TestsforCfile.cpp
 * Author: Анастасия
 *
 * Created on 25.09.2017, 21:10:29
 */

#include "TestsforCfile.h"
#include "../Find_C_file/Find_C_file.h"

CPPUNIT_TEST_SUITE_REGISTRATION(TestsforCfile);

TestsforCfile::TestsforCfile() {
}

TestsforCfile::~TestsforCfile() {
}

void TestsforCfile::setUp() {
}

void TestsforCfile::tearDown() {
}

void TestsforCfile::testError() {
    Find_C_file find;
    string result = find.getlist("");
    if (true /*check result*/) {
        CPPUNIT_ASSERT(result == "File not found");
    }
}

void TestsforCfile::testConfig() {
    Find_C_file find;
    string result = find.getlist("config.log");
    if (true /*check result*/) {
        CPPUNIT_ASSERT(result == "conftest.c ");
    }
}

void TestsforCfile::testAnotherlog() {
    Find_C_file find;
    string result = find.getlist("tests/test.log");
    if (true /*check result*/) {
        CPPUNIT_ASSERT(result == "PACMAN.c Steam.c NVIDIA.c ");
    }
}