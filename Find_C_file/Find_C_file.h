/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Find_C_file.h
 * Author: Анастасия
 *
 * Created on 25 сентября 2017 г., 14:58
 */

#ifndef FIND_C_FILE_H 
#define FIND_C_FILE_H 

#include <iostream>
using namespace std; 

class Find_C_file { 
    public: 
    Find_C_file(); 
    Find_C_file(const Find_C_file& orig); 
    virtual ~Find_C_file(); 
    string getlist(string FileName); 
    private: 

}; 

#endif /* FIND_C_FILE_H */