# Compile
C=g++ -c -O2 -o

neall: main.o Find_C_file.o TestsforCfile.o TestforCfileRunner.o Search Tests Comparing Clean

# Compiling program
main.o: main.cpp
	$(C) main.o main.cpp
Find_C_file.o: Find_C_file/Find_C_file.cpp
	$(C) Find_C_file.o Find_C_file/Find_C_file.cpp
TestsforCfile.o: tests/TestsforCfile.cpp
	$(C) TestsforCfile.o tests/TestsforCfile.cpp
TestforCfileRunner.o: tests/TestforCfileRunner.cpp
	$(C) TestforCfileRunner.o tests/TestforCfileRunner.cpp
Search: main.o Find_C_file.o 
	g++ main.o Find_C_file.o -o Search

# Running tests
Tests: Find_C_file.o TestsforCfile.o TestforCfileRunner.o
	g++ -o Tests  Find_C_file.o TestsforCfile.o TestforCfileRunner.o -lcppunit
Run: Tests
	@./Tests

# Comparing
Comparing: 
	$(eval SCRIPT=$(shell ./script.sh config.log))
	$(eval PROGRAM=$(shell ./Search config.log))
	@if [ $(SCRIPT) != $(PROGRAM) ]; then\
		 echo "EqError";\
	fi;

# Cleaning
Clean:
	@rm -f *.o 
	@rm -f Tests*
	@rm -f Search*
